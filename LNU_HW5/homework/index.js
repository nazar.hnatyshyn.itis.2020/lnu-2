function isEquals(arg1, arg2) {
    return arg1 === arg2;
}

function numberToString(num) {
    return num.toString();
}

function storeNames(...arg) {
    return [...arg];
}

function getDivision(num1, num2) {
    if(isEquals(num1, num2)) {
        return 1;
    }
    return num1 > num2 ? num1 / num2 : num2 / num1;
    
}

function negativeCount(arr) {
    let res = [];
    for(let el of arr) {
        if(el<0) {
            res.push(el);
        }
    }
    return res.length;
}
